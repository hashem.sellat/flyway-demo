# Flyway Demo  
All the migrations are put into sql folder, with baseline 1.0.0.  
To get the database ready to use, we apply the following actions:  
* First we specify the schema base line version.  
	```bash
	./flyway -baselineVersion=1.0.0 baseline
	```   
* Then we perform the migration.  
	```bash
	./flyway migrate
	```  
