create table `oauth_auth_codes` (`id` varchar(100) not null, `user_id` bigint not null, `client_id` int unsigned not null, `scopes` text null, `revoked` tinyint(1) not null, `expires_at` datetime null) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

alter table `oauth_auth_codes` add primary key `oauth_auth_codes_id_primary`(`id`);

create table `oauth_access_tokens` (`id` varchar(100) not null, `user_id` bigint null, `client_id` int unsigned not null, `name` varchar(255) null, `scopes` text null, `revoked` tinyint(1) not null,`created_at` timestamp null, `updated_at` timestamp null, `expires_at` datetime null) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

alter table `oauth_access_tokens` add primary key `oauth_access_tokens_id_primary`(`id`);

alter table `oauth_access_tokens` add index `oauth_access_tokens_user_id_index`(`user_id`);

create table `oauth_refresh_tokens` (`id` varchar(100) not null, `access_token_id` varchar(100) not null, `revoked` tinyint(1) not null, `expires_at` datetime null) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

alter table `oauth_refresh_tokens` add primary key `oauth_refresh_tokens_id_primary`(`id`);

alter table `oauth_refresh_tokens` add index `oauth_refresh_tokens_access_token_id_index`(`access_token_id`);

create table `oauth_clients` (`id` int unsigned not null auto_increment primary key, `user_id` bigint null, `name` varchar(255) not null, `secret` varchar(100) null, `redirect` text not null, `personal_access_client` tinyint(1) not null, `password_client` tinyint(1) not null, `revoked` tinyint(1) not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

alter table `oauth_clients` add index `oauth_clients_user_id_index`(`user_id`);

create table `oauth_personal_access_clients` (`id` int unsigned not null auto_increment primary key, `client_id` int unsigned not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

alter table `oauth_personal_access_clients` add index `oauth_personal_access_clients_client_id_index`(`client_id`);
